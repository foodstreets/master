package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"sync"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	once sync.Once

	// Used for flags.
	rootConfigFile   string
	rootCmd          *cobra.Command
	rootViper        = viper.New()
	customConfigFile string
	customViper      = viper.New()
)

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initCustomConfig, initRootConfig)
	rootCmd = &cobra.Command{
		Use:   "CLI",
		Short: "Some help",
		// Uncomment the following line if your bare application
		// has an action associated with it:
		Run: func(cmd *cobra.Command, args []string) {},
	}

	pflags := rootCmd.PersistentFlags()
	pflags.StringVar(&customConfigFile, "config", "config/config.yaml", "custom config file")

	defaultFlags()
}

func defaultFlags() {
	pflags := rootCmd.PersistentFlags()

	//Setup port flag --port=xxx
	pflags.Int("port", 9091, "app binding port")
	customViper.BindPFlag("app.port", pflags.Lookup("port"))
}

func initCustomConfig() {
	if customConfigFile != "" {
		customViper.SetConfigFile(customConfigFile)
		if err := customViper.ReadInConfig(); err != nil {
			fmt.Println("WARNING: file config/config.yaml not exist")
		}
	}
}

func initRootConfig() {
	envGoPath := os.Getenv("GOPATH")
	goPaths := filepath.SplitList(envGoPath)
	if len(goPaths) == 0 {
		panic("$GOPATH is not set")
	}
	for _, goPath := range goPaths {
		configDir := filepath.Join(goPath, "src", "master", "config")
		rootViper.AddConfigPath(configDir)
	}
	rootViper.SetConfigName("config")

	if err := rootViper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", rootViper.ConfigFileUsed())
	}
}

func GetViper() *viper.Viper {
	once.Do(func() {
		for _, key := range customViper.AllKeys() {
			rootViper.Set(key, customViper.Get(key))
		}
	})
	return rootViper
}
