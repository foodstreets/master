package config

import (
	"sync"

	"gitlab.com/foodstreets/master/cmd"
)

var (
	conf Config
	once sync.Once
)

type Config struct {
	App          App
	Postgresql   Postgresql
	Elastic      ElasticConf
	Redis        RedisConf
	GrpcAuth     GrpcRawConf
	GrpcNewsfeed GrpcRawConf
}

type Postgresql struct {
	Host     string
	Port     string
	Username string
	Password string
	Dbname   string
	Debug    bool
}

type ElasticConf struct {
	Host  string
	Port  int
	Index string
}

type RedisConf struct {
	Address  string
	Password string
}

type GrpcRawConf struct {
	Url string
}

type App struct {
	Host string
	Port int
}

func load() {
	once.Do(func() {
		cmd.GetViper().Unmarshal(&conf)
	})
}

func Load() Config {
	load()
	return conf
}

func Get() Config {
	load()
	return conf
}
