
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE districts(
	id serial NOT NULL PRIMARY KEY,
	city_id INTEGER,	
	name TEXT NOT NULL
);

CREATE INDEX districts_city_id_idx ON districts USING btree (city_id);
-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE districts;

