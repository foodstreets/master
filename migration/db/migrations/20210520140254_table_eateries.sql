
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE eateries (
	id serial NOT NULL PRIMARY KEY,
	name TEXT NOT NULL,
	slug TEXT,
	active boolean,
	address TEXT,
	image TEXT,
	city_id INTEGER,
	district_id INTEGER,
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL,
	created_by INTEGER NOT NULL,
	updated_by INTEGER NULL
);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE eateries;

