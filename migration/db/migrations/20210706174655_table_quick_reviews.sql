
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE quick_reviews(
	id serial NOT NULL PRIMARY KEY,
	type_name TEXT,
	image TEXT,
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL
);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE quick_reviews;
