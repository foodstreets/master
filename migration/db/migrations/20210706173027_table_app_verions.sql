
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TYPE app_version_method_type_enum as enum('normal', 'update', 'force_update');
CREATE TABLE app_versions(
	id serial NOT NULL PRIMARY KEY,
	version TEXT,
	title TEXT,
	message TEXT NOT NULL,
	"type" app_version_method_type_enum,
	build_number INTEGER,
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL
);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE app_versions;
