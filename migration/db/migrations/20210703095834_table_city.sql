
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE cities(
	id serial NOT NULL PRIMARY KEY,
	name TEXT NOT NULL,
	slug TEXT
);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE cities;

