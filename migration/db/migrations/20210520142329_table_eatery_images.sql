
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE eatery_images(
	id serial NOT NULL PRIMARY KEY,
	eatery_id INTEGER NOT NULL,
	images TEXT[],
	created_at timestamptz NULL DEFAULT now(),
	updated_at timestamptz NULL,
	created_by INTEGER NOT NULL,
	updated_by INTEGER NULL
);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE eatery_images;

