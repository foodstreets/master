
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE users (
	id serial NOT NULL,
	username varchar(100) NULL DEFAULT NULL::character varying,
	avatar varchar(510) NULL DEFAULT NULL::character varying,
	"last" varchar(200) NULL DEFAULT NULL::character varying,
	middle varchar(200) NULL DEFAULT NULL::character varying,
	"first" varchar(200) NULL DEFAULT NULL::character varying,
	gender varchar(60) NULL DEFAULT NULL::character varying,
	email varchar(200) NULL DEFAULT NULL::character varying,
	phone_number varchar(60) NULL DEFAULT NULL::character varying,
	birthday varchar(200) NULL DEFAULT NULL::character varying,
	timezone int4 NULL,
	street varchar(200) NULL DEFAULT NULL::character varying,
	created_at timestamptz NULL DEFAULT 'now'::text::date,
	created_by int4 NULL,
	updated_at timestamptz NULL DEFAULT 'now'::text::date,
	updated_by int4 NULL,
	"status" int4 NULL DEFAULT 0,
	city_id int4 NULL DEFAULT 0,
	active int4 NOT NULL DEFAULT 0,
	"password" text NULL,
	device text NULL DEFAULT ''::text,
	platform text NULL DEFAULT ''::text,
	verified_phone_number bool NULL,
	token_hash text
);

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE users;

