
-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE user_accounts (
	id serial NOT NULL,
	provider varchar(510) NULL DEFAULT NULL::character varying,
	uid varchar(100) NULL DEFAULT NULL::character varying,
	username varchar(200) NULL DEFAULT NULL::character varying,
	profile_url varchar(510) NULL DEFAULT NULL::character varying,
	access_token varchar(2000) NULL DEFAULT NULL::character varying,
	refresh_token varchar(2000) NULL DEFAULT NULL::character varying,
	created_at timestamptz NULL DEFAULT 'now'::text::date,
	updated_at timestamptz NULL DEFAULT 'now'::text::date,
	"user_id" int4 NULL,
	"status" int4 NULL DEFAULT 0,
	country_code varchar(10) NULL,
	phone_number varchar(30) NULL,
	last_login_at timestamptz NOT NULL DEFAULT 'now'::text::date,
	token_expired_at timestamptz NOT NULL DEFAULT now(),
	CONSTRAINT user_accounts_pkey PRIMARY KEY (id)
);
CREATE INDEX idx_user_accounts_phone_number ON user_accounts USING btree (phone_number);
CREATE INDEX idx_user_accounts_status ON user_accounts USING btree (status);
CREATE INDEX user_accounts_country_code_phone_number_idx ON user_accounts USING btree (country_code, phone_number);
CREATE INDEX user_accounts_provider_idx ON user_accounts USING btree (provider);
CREATE INDEX user_accounts_uid_idx ON user_accounts USING btree (uid);
CREATE INDEX user_accounts_user_idx ON user_accounts USING btree ("user_id");
CREATE INDEX user_accounts_user_provider_idx ON user_accounts USING btree ("user_id", provider);


-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE user_accounts;
