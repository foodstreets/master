package tasks

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	newsfeedClient "newsfeed/init/client"
	newsfeedRequest "newsfeed/protobuf/request"
	"os"
	"time"

	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/orm"

	"gitlab.com/foodstreets/master/model"
)

const (
	delayTime = 100
)

type eatery struct{}

var Eatery eatery

type Merchant struct {
	Reply  ArrReply `json:"reply"`
	Result string   `json:"result"`
}

type ArrReply struct {
	DeliveryInfors []DeliveryInfos `json:"delivery_infos"`
}

type Position struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type Operating struct {
	Opentime  string `json:"open_time"`
	CloseTime string `json:"close_time"`
}

type Photos struct {
	Witdth int    `json:"width"`
	Height int    `json:"height"`
	Value  string `json:"value"`
}

type Brand struct {
	Operating Operating `json:"operating"`
}

type DeliveryInfos struct {
	Name          string    `json:"name"`
	Address       string    `json:"address"`
	IsOpen        bool      `json:"is_open"`
	Phones        []string  `json:"phones"`
	Photos        []Photos  `json:"photos"`
	Url           string    `json:"url"`
	Position      Position  `json:"position"`
	Brand         Brand     `json:"brand"`
	Operating     Operating `json:"operating"`
	RestaurantUrl string    `json:"restaurant_url"`
}

func (eatery) UpdateEateryLocation() (err error) {
	nextId := 0
	limit := 200
	for {
		sql := `SELECT * from eatery_address
			WHERE id > ?
			ORDER BY id ASC
			LIMIT ?
		`
		eateryAddresss := []model.EateryAddress{}
		_, err := infra.GetDB().Query(&eateryAddresss, sql, nextId, limit)
		if err != nil {
			return err
		}
		if len(eateryAddresss) == 0 {
			break
		}

		ids := make([]int, len(eateryAddresss))
		for i, eateryAddress := range eateryAddresss {
			ids[i] = eateryAddress.EateryId
		}

		err = newsfeedClient.Eatery.UpdateLocationIndexByIds(ids)
		if err != nil {
			panic(err)
			continue
		}

		nextId = eateryAddresss[len(eateryAddresss)-1].Id
		fmt.Println("DONE....%d", len(eateryAddresss))
		fmt.Println("NextId....%d", nextId)
		time.Sleep(delayTime)
	}
	return
}

func (eatery) CreateEateryIndex() (err error) {
	nextId := 0
	limit := 200
	for {
		sql := `SELECT * FROM eateries 
		WHERE id > ? 
		ORDER BY id ASC
		LIMIT ?`
		eateries := []model.Eatery{}
		_, err := infra.GetDB().Query(&eateries, sql, nextId, limit)
		if err != nil {
			return err
		}
		if len(eateries) == 0 {
			break
		}

		for _, eatery := range eateries {
			eateryClient := newsfeedRequest.Eatery{
				Id:     int64(eatery.Id),
				Active: eatery.Active,
				CityId: int64(eatery.CityId),
			}

			_, err := newsfeedClient.Eatery.Create(&eateryClient)
			if err != nil {
				panic(err)
				continue
			}

			fmt.Println("eatery", eatery)
		}
		nextId = eateries[len(eateries)-1].Id
		fmt.Println("DONE....%d", len(eateries))
		fmt.Println("NextId....%d", nextId)
		time.Sleep(delayTime)
	}
	return

}

func (eatery) CreateEatery() (err error) {
	//Open our jsonFile
	jsonFile, err := os.Open("tasks/merchant-food-HCM3.json")
	if err != nil {
		panic(err)
		return
	}

	fmt.Println("Ping")
	defer jsonFile.Close()

	byteValues, _ := ioutil.ReadAll(jsonFile)

	var data Merchant

	json.Unmarshal(byteValues, &data)

	merchants := data.Reply.DeliveryInfors

	for _, merchant := range merchants {
		eatery := model.Eatery{
			Name:      merchant.Name,
			Address:   merchant.Address,
			Slug:      merchant.RestaurantUrl,
			CityId:    1,
			Active:    true,
			CreatedBy: 1,
		}

		err = orm.Eatery.Create(&eatery)
		if err != nil {
			panic(err)
			return
		}

		eateryAddress := model.EateryAddress{
			EateryId:  eatery.Id,
			Lat:       merchant.Position.Latitude,
			Lng:       merchant.Position.Longitude,
			CreatedBy: 1,
		}
		err = orm.EateryAddress.Create(&eateryAddress)
		if err != nil {
			panic(err)
			return
		}
	}

	return
}
