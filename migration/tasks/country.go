package tasks

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/foodstreets/master/orm"

	"gitlab.com/foodstreets/master/model"
)

type city struct{}

var City city

type OCity struct {
	Reply  Reply  `json:"reply"`
	Result string `json:"result"`
}

type Reply struct {
	Metadata Metadata `json:"metadata"`
}

type Metadata struct {
	Province []Province `json:"province"`
}

type Province struct {
	Name     string     `json:"name"`
	NameUrl  string     `json:"name_url"`
	District []District `json:"district"`
}

type District struct {
	Name string `json:"name"`
}

func (city) CreateCity() (err error) {
	//Open our jsonFile
	jsonFile, err := os.Open("tasks/metadata.json")
	if err != nil {
		panic(err)
		return
	}

	fmt.Println("Ping")
	defer jsonFile.Close()

	byteValues, _ := ioutil.ReadAll(jsonFile)

	var data OCity

	json.Unmarshal(byteValues, &data)

	provinces := data.Reply.Metadata.Province
	for _, province := range provinces {
		city := model.City{
			Name: province.Name,
			Slug: province.NameUrl,
		}
		err := orm.City.Create(&city)
		if err != nil {
			continue
		}

		for _, district := range province.District {
			district := model.District{
				Name:   district.Name,
				CityId: city.Id,
			}
			err := orm.District.Create(&district)
			if err != nil {
				continue
			}
		}
	}

	fmt.Println("provinces", provinces)

	return
}
