package main

import (
	"fmt"
	"master/migration/tasks"
	newsfeedClient "newsfeed/init/client"
)

func init() {
	address := "localhost:5001"
	newsfeedClient.Init(address)
}

func main() {
	eatery := tasks.Eatery.UpdateEateryLocation()
	//	eatery := tasks.Eatery.CreateEateryIndex()
	//	eatery := tasks.Eatery.CreateEatery()
	//	city := tasks.City.CreateCity()
	fmt.Println("eatery", eatery)
}
