package redis

import (
	"time"

	"gitlab.com/foodstreets/master/model"
)

type User struct {
	Id                  int       `json:"id"`
	Username            string    `json:"username"`
	Avatar              string    `json:"avatar"`
	Last                string    `json:"last"`
	Middle              string    `json:"middle"`
	First               string    `json:"first"`
	Gender              string    `json:"gender"`
	Email               string    `json:"email"`
	PhoneNumber         string    `json:"phoneNumber"`
	Birthday            string    `json:"birthday"`
	Timezone            time.Time `json:"timezone"`
	Street              string    `json:"street"`
	CreatedAt           time.Time `json:"createdAt"`
	UpdatedAt           time.Time `json:"updatedAt"`
	CreatedBy           int       `json:"createdBy"`
	UpdatedBy           int       `json:"updatedBy"`
	Status              string    `json:"status"`
	CityId              int       `json:"cityId"`
	Active              bool      `json:"active"`
	Device              string    `json:"device"`
	Platform            string    `json:"platform"`
	VerifiedPhoneNumber bool      `json:"verifiedPhoneNumber"`
	Session             string    `json:"session"`
}

func ParseToUser(user *model.User) User {
	if user == nil {
		return User{}
	}

	return User{
		Id:                  user.Id,
		Username:            user.Username,
		Avatar:              user.Avatar,
		Last:                user.Last,
		Middle:              user.Middle,
		First:               user.First,
		Gender:              user.Gender,
		Email:               user.Email,
		PhoneNumber:         user.PhoneNumber,
		Birthday:            user.Birthday,
		Status:              user.Status,
		CityId:              user.CityId,
		Active:              user.Active,
		Device:              user.Device,
		Platform:            user.Platform,
		VerifiedPhoneNumber: user.VerifiedPhoneNumber,
		Session:             user.TokenHash,
	}
}
