package infra

import (
	"context"
	"fmt"

	"github.com/go-redis/redis/v8"
	"gitlab.com/foodstreets/master/config"
)

var (
	rdb *redis.Client
	Ctx = context.Background()
)

const (
	maxConnAge      = 365 * 24 * 60 * 60
	maxretryBackoff = 60 * 60
	poolSize        = 200
	idleTimeout     = 60
)

type RedisConf struct {
	Addr     []string
	Username string
	Password string
}

func Redis() *redis.Client {
	if rdb == nil {
		panic("Redis disconnect !!!")
	}
	return rdb
}

func InitRedis(conf config.RedisConf) *redis.Client {
	fmt.Println("DEBUG", conf)
	rdb = redis.NewClient(&redis.Options{
		Addr:     conf.Address,
		Password: conf.Password,
	})

	_, err := rdb.Ping(Ctx).Result()
	if err != nil {
		fmt.Println("ERR", err)
		panic(err)
	}
	return rdb
}

func CloseRedis() {
	if rdb != nil {
		if err := rdb.Close(); err != nil {
			panic(fmt.Sprintf("Redis close error: %s", err.Error()))
		}
	}
}
