package infra

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"

	"github.com/olivere/elastic/v7"
)

var (
	client            *elastic.Client
	ErrConnectElastic = errors.New("Elastic not connect")
)

type ElasticConf struct {
	Host  string
	Port  int
	Index string
}

func Elastic() *elastic.Client {
	if client == nil {
		panic(ErrConnectElastic)
	}
	return client
}

func InitElastic(conf *ElasticConf) {
	var (
		address string                     = fmt.Sprintf("http://%s:%d", conf.Host, conf.Port)
		options []elastic.ClientOptionFunc = []elastic.ClientOptionFunc{elastic.SetURL(address)}
		err     error
	)

	client, err = elastic.NewClient(options...)
	if err != nil {
		panic(err)
	}

	// Use the IndexExists service to check if a specified index exists.
	exists, err := client.IndexExists(conf.Index).Do(context.Background())
	if err != nil {
		panic(err)
	}
	if !exists {
		link := "./config/elastic/mapping.json"
		mapping, _ := ioutil.ReadFile(string(link))
		_, err = client.CreateIndex(conf.Index).Body(string(mapping)).Do(context.Background())
		if err != nil {
			panic(err)
		}
	}
}
