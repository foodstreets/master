package infra

import (
	"sync"

	"github.com/go-pg/pg"
	"gitlab.com/foodstreets/master/config"
)

var once sync.Once

type PGDB struct {
	*pg.DB
}

var singleDB *PGDB

func InitMaster(psql config.Postgresql) *PGDB {
	if singleDB == nil {
		once.Do(
			func() {
				db := pg.Connect(&pg.Options{
					Addr:     psql.Host + ":" + psql.Port,
					User:     psql.Username,
					Password: psql.Password,
					Database: psql.Dbname,
				})
				singleDB = &PGDB{db}

			})
	}
	return singleDB
}

func GetDB() *PGDB {
	if singleDB == nil {
		panic("ERR")
	}

	return singleDB
}
