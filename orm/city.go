package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"
)

type city struct{}

var City ICity

func init() {
	City = &city{}
}

type ICity interface {
	Create(city *model.City) error
}

func (c city) Create(city *model.City) error {
	return infra.GetDB().Insert(city)
}
