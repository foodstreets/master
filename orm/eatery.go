package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"

	"github.com/go-pg/pg"
)

type eatery struct{}

var Eatery IEatery

func init() {
	Eatery = &eatery{}
}

type IEatery interface {
	Create(eatery *model.Eatery) error
	CreateMulti(eateries []model.Eatery) error
	GetByID(id int) (*model.Eatery, error)
	GetByIDs(ids []int) (int, []model.Eatery, error)
	GetBySlug(slug string) (*model.Eatery, error)
}

func (e eatery) Create(eatery *model.Eatery) error {
	return infra.GetDB().Insert(eatery)
}

func (e eatery) CreateMulti(eateries []model.Eatery) (err error) {
	if len(eateries) == 0 {
		return
	}
	return infra.GetDB().Insert(&eateries)
}

func (e eatery) GetByID(id int) (*model.Eatery, error) {
	eatery := model.Eatery{}
	err := infra.GetDB().Model(&eatery).
		Where("id = ?", id).
		Order("id DESC").
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return &eatery, err
}

func (e eatery) GetBySlug(slug string) (eatery *model.Eatery, err error) {
	err = infra.GetDB().Model(&eatery).
		Where("slug = ?", slug).
		Order("id DESC").
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return
}

func (e eatery) GetByIDs(ids []int) (int, []model.Eatery, error) {
	var eateries []model.Eatery
	query := infra.GetDB().Model(&eateries)
	total, err := query.Order("id DESC").
		Offset(0).
		Limit(20).
		SelectAndCount()
	return total, eateries, err
}
