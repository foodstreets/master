package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"

	"github.com/go-pg/pg"
)

type groupDish struct {
}

var GroupDish IGroupDish

func init() {
	GroupDish = &groupDish{}
}

type IGroupDish interface {
	Create(groupDish *model.GroupDish) error
	GetByID(id int) (*model.GroupDish, error)
}

func (g groupDish) Create(groupDish *model.GroupDish) error {
	return infra.GetDB().Insert(groupDish)
}

func (g groupDish) GetByID(id int) (groupDish *model.GroupDish, err error) {
	err = infra.GetDB().Model(&groupDish).
		Where("id = ?", id).
		Order("id DESC").
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return
}
