package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"
)

type district struct{}

var District IDistrict

func init() {
	District = &district{}
}

type IDistrict interface {
	Create(district *model.District) error
}

func (d district) Create(district *model.District) error {
	return infra.GetDB().Insert(district)
}
