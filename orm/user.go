package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"

	"github.com/go-pg/pg/v10"
)

type user struct{}

var User IUser

func init() {
	User = &user{}
}

type IUser interface {
	GetByUserId(userId int) (*model.User, error)
	Create(user *model.User) error
	Update(user *model.User) error
	GetByEmail(email string) (*model.User, error)
	GetByID(ID int) (*model.User, error)
	GetByPhoneNumber(phoneNumber string) (*model.User, error)
	GetByPhoneEmail(phoneNumber, email string) (*model.User, error)
}

func (u user) GetByUserId(userId int) (user *model.User, err error) {
	err = infra.GetDB().Model(&user).
		Where("id = ?", userId).
		Order("id DESC").
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return
}

func (u user) Create(user *model.User) error {
	return infra.GetDB().Insert(user)
}

func (u user) Update(user *model.User) error {
	return infra.GetDB().Update(user)
}

func (u user) GetByEmail(email string) (user *model.User, err error) {
	err = infra.GetDB().Model(&user).
		Where("email = ?", email).
		Order("id DESC").
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return
}

func (u user) GetByID(ID int) (user *model.User, err error) {
	err = infra.GetDB().Model(&user).
		Where("id = ?", ID).
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return
}

func (u user) GetByPhoneNumber(phoneNumber string) (*model.User, error) {
	user := &model.User{}
	err := infra.GetDB().Model(user).
		Where("phone_number = ?", phoneNumber).
		Order("id DESC").
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return user, err
}

func (u user) GetByPhoneEmail(phoneNumber, email string) (*model.User, error) {
	user := &model.User{}
	err := infra.GetDB().Model(user).
		Where("phone_number = ?", phoneNumber).
		Where("email = ?", email).
		Order("id DESC").
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return user, err
}
