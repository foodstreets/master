package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"

	"github.com/go-pg/pg"
)

type dish struct{}

var Dish IDish

func init() {
	Dish = &dish{}
}

type IDish interface {
	Create(dish *model.Dish) error
	GetByID(id int) (*model.Dish, error)
}

func (d dish) Create(dish *model.Dish) error {
	return infra.GetDB().Insert(dish)
}

func (d dish) GetByID(id int) (*model.Dish, error) {
	var dishes model.Dish
	err := infra.GetDB().Model(&dishes).
		Where("id = ?", id).
		Order("id DESC").
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return &dishes, nil
}
