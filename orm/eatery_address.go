package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"

	"github.com/go-pg/pg"
)

type eateryAddress struct{}

var EateryAddress IEateryAddress

func init() {
	EateryAddress = &eateryAddress{}
}

type IEateryAddress interface {
	Create(eateryAddress *model.EateryAddress) error
	GetByEateryIds(eateryIds []int) ([]model.EateryAddress, error)
}

func (e eateryAddress) Create(eateryAddress *model.EateryAddress) error {
	return infra.GetDB().Insert(eateryAddress)
}

func (e eateryAddress) GetByEateryIds(eateryIds []int) ([]model.EateryAddress, error) {
	var eateryAddresss []model.EateryAddress
	err := infra.GetDB().Model(&eateryAddresss).
		Where("eatery_id IN (?)", pg.In(eateryIds)).
		Select()
	return eateryAddresss, err
}
