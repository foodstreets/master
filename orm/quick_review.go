package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"
)

type quickReview struct{}

var QuickReview IQuickReview

func init() {
	QuickReview = &quickReview{}
}

type IQuickReview interface {
	Create(quickReview *model.QuickReview) error
}

func (q quickReview) Create(quickReview *model.QuickReview) error {
	return infra.GetDB().Insert(quickReview)
}
