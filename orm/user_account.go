package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"

	"github.com/go-pg/pg"
)

type userAccount struct{}

var UserAccount IUserAccount

type IUserAccount interface {
	GetByUID(uid string) (*model.UserAccount, error)
	Update(userAccount *model.UserAccount) error
}

func init() {
	UserAccount = &userAccount{}
}

func (userAccount) GetByUID(uid string) (userAccount *model.UserAccount, err error) {
	err = infra.GetDB().Model(&userAccount).
		Where("uid = ?", uid).
		Order("id DESC").
		Limit(1).
		Select()

	if err == pg.ErrNoRows {
		return nil, nil
	}
	return
}

func (userAccount) Update(userAccount *model.UserAccount) error {
	return infra.GetDB().Update(userAccount)
}
