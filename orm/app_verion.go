package orm

import (
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/model"
)

type appVersion struct{}

var AppVersion IAppVersion

type AppVersionParam struct {
	Version     string
	Title       string
	Message     string
	Type        string
	BuildNumber int
}

func init() {
	AppVersion = &appVersion{}
}

type IAppVersion interface {
	Create(appVersion *model.AppVersion) error
}

func (a appVersion) Create(appVersion *model.AppVersion) (err error) {
	return infra.GetDB().Insert(appVersion)
}
