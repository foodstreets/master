package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	userKey = "currentUser"
)

type CurrentUserFunc func(idStr string) (interface{}, error)
type GetCurrentUser func(c *gin.Context) (interface{}, bool)

type IAuthMiddleware interface {
	GetCurrentUser(c *gin.Context) (interface{}, bool)
	Interception() gin.HandlerFunc
}

type authenMiddleware struct {
	secure      *SecureCookie
	currentUser CurrentUserFunc
}

func NewAuthenMiddleware(securecookie *SecureCookie, currentUser CurrentUserFunc) IAuthMiddleware {
	return &authenMiddleware{
		secure:      securecookie,
		currentUser: currentUser,
	}
}

func (a *authenMiddleware) GetCurrentUser(c *gin.Context) (interface{}, bool) {
	user, exists := c.Get(userKey)
	return user, exists
}

func (a *authenMiddleware) Interception() gin.HandlerFunc {
	return func(c *gin.Context) {
		isLogged := true
		userId, err := a.secure.GetCurrentUserID(c.Request)
		if err != nil {
			if err != http.ErrNoCookie {
				//				if err.Error() == "securecookie: expired timestamp" {
				//					a.secure.ClearCookie(c.Writer, AccessToken)
				//				} else if err.Error() == "securecookie: the value is not valid" {
				//					a.secure.ClearCookie(c.Writer, AccessToken)
				//				}
				a.secure.ClearCookie(c.Writer, AccessToken)
			}
			isLogged = false
		}
		if isLogged {
			user, err := a.currentUser(userId)
			if err != nil {
				a.secure.ClearCookie(c.Writer, AccessToken)
				c.AbortWithStatus(http.StatusForbidden)
				return
			}
			c.Set(userKey, user)
		}

		// Before request
		c.Next()
	}
}
