package middleware

import (
	"net/http"

	"github.com/gorilla/securecookie"
)

const (
	ExpireTimeSeconds = 365 * 24 * 60 * 60 // 1 year
	AccessToken       = "accessToken"
	MaxAge            = ExpireTimeSeconds
)

type SecureCookie struct {
	securecookie *securecookie.SecureCookie
}

func NewSecureCookie(blockKey, hashKey string) *SecureCookie {
	byteBlockKey := []byte(blockKey)
	byeteHashKey := []byte(hashKey)
	secure := securecookie.New(byteBlockKey, byeteHashKey)
	secure.MaxAge(ExpireTimeSeconds)
	return &SecureCookie{secure}
}

func (s *SecureCookie) GetCurrentUserID(req *http.Request) (string, error) {
	token := req.Header.Get("X-Access-Token")
	if token == "" {
		return s.DecodeToken(AccessToken, req)
	}

	value := ""
	err := s.securecookie.Decode(AccessToken, token, &value)
	return value, err
}

func (s *SecureCookie) DecodeToken(accessToken string, req *http.Request) (string, error) {
	value := ""
	cookie, err := req.Cookie(accessToken)
	if err != nil {
		return "", err
	}

	err = s.securecookie.Decode(accessToken, cookie.Value, &value)
	return value, err
}

func (s *SecureCookie) ClearCookie(rw http.ResponseWriter, name string) {
	c := &http.Cookie{
		Name:     name,
		Value:    "",
		Path:     "/",
		MaxAge:   -1,
		HttpOnly: true,
	}

	http.SetCookie(rw, c)
}

func (s *SecureCookie) SetAuthentication(cookieName string, value int, path string, w http.ResponseWriter) (encoded string, err error) {
	s.securecookie.MaxAge(MaxAge)
	encoded, err = s.securecookie.Encode(cookieName, value)
	if err != nil {
		return
	}
	cookie := &http.Cookie{
		Name:     cookieName,
		Value:    encoded,
		Path:     path,
		MaxAge:   MaxAge,
		HttpOnly: true,
	}
	http.SetCookie(w, cookie)
	return
}
