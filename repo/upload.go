package repo

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/jmcvetta/napping"
)

type upload struct{}

var Upload IUpload

type IUpload interface {
	MoveImageFood(address, cdnName, newName string) error
	GetImageFoodURL(address, imageName string) string
}

func init() {
	Upload = &upload{}
}

func (u upload) MoveImageFood(address, cdnName, newName string) error {
	url := address + "/upload/images/" + cdnName + "/move?name=" + newName
	return moveImage(cdnName, newName, url)
}

func (u upload) GetImageFoodURL(address, imageName string) string {
	return fmt.Sprintf("%s/food/image/%s", address, imageName)
}

func moveImage(cdnName, newName, url string) error {
	var errRes error
	s := napping.Session{}
	header := http.Header{}
	header.Add("Content-type", "application/json")
	s.Header = &header
	resp, err := s.Post(url, nil, nil, &errRes)
	if err != nil {
		msg := fmt.Sprintf(`MoveImage, "%s" -> "%s", err: %v`, cdnName, newName, err)
		fmt.Println(msg)
		return errors.New(msg)
	}
	if errRes != nil {
		msg := fmt.Sprintf(`MoveImage, "%s" -> "%s", errRes: %v`, cdnName, newName, errRes)
		fmt.Println(msg)
		return errors.New(msg)
	}
	if resp.Status() != 200 {
		msg := fmt.Sprintf(`MoveImage, failed to move image, "%s" -> "%s", status: %v`, cdnName, newName, resp.Status())
		fmt.Println(msg)
		return errors.New(msg)
	}
	return nil
}
