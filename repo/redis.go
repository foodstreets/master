package repo

import (
	"encoding/json"
	"time"

	"gitlab.com/foodstreets/master/infra"

	"github.com/go-redis/redis/v8"
)

type rRedis struct{}

var (
	Redis IRedis
	ctx   = infra.Ctx
)

type IRedis interface {
	Get(key string, src interface{}) error
	GetBytes(key string) ([]byte, error)
	Set(key string, value interface{}, expiration time.Duration) error
	Delete(keys []string) error
	Refresh(key string, expiration time.Duration) error
	MGet(keys []string) ([]interface{}, error)
	MSet(keys []string, values []interface{}, expiration time.Duration) error
	LPush(key string, value interface{}, expiration time.Duration) error
	SAdd(key string, value interface{}, expiration time.Duration) error
}

func init() {
	Redis = &rRedis{}
}

func (r rRedis) Get(key string, src interface{}) (err error) {
	value, err := infra.Redis().Get(ctx, key).Result()
	if err != nil {
		if err == redis.Nil {
			return nil
		}
		return
	}

	err = json.Unmarshal([]byte(value), &src)
	return
}

func (r rRedis) GetBytes(key string) (blob []byte, err error) {
	blob, err = infra.Redis().Get(ctx, key).Bytes()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return
	}

	return
}

func (r rRedis) Set(key string, value interface{}, expiration time.Duration) (err error) {
	cacheEntry, err := json.Marshal(value)
	if err != nil {
		return
	}

	return infra.Redis().Set(ctx, key, cacheEntry, expiration).Err()
}

func (r rRedis) SAdd(key string, value interface{}, expiration time.Duration) (err error) {
	cacheEntry, err := json.Marshal(value)
	if err != nil {
		return
	}

	pipe := infra.Redis().TxPipeline()
	pipe.Expire(ctx, key, expiration)
	if err := infra.Redis().SAdd(ctx, key, cacheEntry).Err(); err != nil {
		return err
	}
	if _, err := pipe.Exec(ctx); err != nil {
		return err
	}
	return
}

func (r rRedis) LPush(key string, value interface{}, expiration time.Duration) (err error) {
	cacheEntry, err := json.Marshal(value)
	if err != nil {
		return
	}

	pipe := infra.Redis().TxPipeline()
	pipe.Expire(ctx, key, expiration)
	if err := infra.Redis().LPush(ctx, key, cacheEntry).Err(); err != nil {
		return err
	}
	if _, err := pipe.Exec(ctx); err != nil {
		return err
	}
	return
}

func (r rRedis) Delete(keys []string) error {
	return infra.Redis().Del(ctx, keys...).Err()
}

func (r rRedis) Refresh(key string, expiration time.Duration) error {
	return infra.Redis().Expire(ctx, key, expiration).Err()
}

func (r rRedis) MSet(keys []string, values []interface{}, expiration time.Duration) (err error) {
	var ifaces []interface{}
	pipe := infra.Redis().TxPipeline()
	for i := range keys {
		ifaces = append(ifaces, keys[i], values[i])
		pipe.Expire(ctx, keys[i], expiration)
	}

	if err := infra.Redis().MSet(ctx, ifaces...).Err(); err != nil {
		return err
	}
	if _, err := pipe.Exec(ctx); err != nil {
		return err
	}

	return
}

func (r rRedis) MGet(keys []string) ([]interface{}, error) {
	return infra.Redis().MGet(ctx, keys...).Result()
}
