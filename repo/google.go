package repo

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/foodstreets/master/model"
)

type google struct {
}

var Google IGoogle

const (
	Url = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="
)

type IGoogle interface {
	GetUserByAccessToken(accessToken string) (*model.Google, error)
}

func init() {
	Google = &google{}
}

func (g google) GetUserByAccessToken(accessToken string) (*model.Google, error) {
	response, err := http.Get(Url + accessToken)
	if err != nil {
		return nil, fmt.Errorf("failed getting user info: %s", err.Error())
	}

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("failed reading response body: %s", err.Error())
	}

	var mGoogle model.Google
	err = json.Unmarshal(contents, &mGoogle)
	return &mGoogle, err
}
