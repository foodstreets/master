package repo

import (
	"gitlab.com/foodstreets/master/model"

	fb "github.com/huandu/facebook"
)

const (
	SDKVersion = "v4.0"
)

type facebook struct{}

var Facebook IFacebook

type IFacebook interface {
	GetUserByAccessToken(accessToken string) (*model.Facebook, error)
}

func init() {
	Facebook = &facebook{}
}

func (facebook) GetUserByAccessToken(accessToken string) (*model.Facebook, error) {
	session := &fb.Session{}
	session.Version = SDKVersion
	params := fb.Params{
		"access_token": accessToken,
		"fields":       "name, first_name, last_name, email, middle_name, name_format",
	}
	res, err := session.Get("/me", params)
	if err != nil {
		return nil, err
	}

	user, err := parseUser(res)
	return &user, err

}

func parseUser(fbResult fb.Result) (user model.Facebook, err error) {
	err = fbResult.DecodeField("id", &user.Uid)
	if err != nil {
		return
	}
	err = fbResult.DecodeField("name", &user.Name)
	if err != nil {
		return
	}

	// Optional fields
	fbResult.DecodeField("last_name", &user.LastName)
	fbResult.DecodeField("middle_name", &user.MiddleName)
	fbResult.DecodeField("first_name", &user.FirstName)
	fbResult.DecodeField("gender", &user.Gender)
	fbResult.DecodeField("email", &user.Email)
	fbResult.DecodeField("birthday", &user.Birthday)
	fbResult.DecodeField("name_format", &user.NameFormat)
	return
}
