package repo

import (
	"context"
	"time"

	"gitlab.com/foodstreets/master/model"

	iapple "github.com/Timothylock/go-signin-with-apple/apple"
)

type apple struct {
}

var Apple IApple

type IApple interface {
	GetUserByAccessToken(accessToken, sdkType string) (model.Apple, error)
}

func init() {
	Apple = &apple{}
}

type SDKType struct {
	ClientID   string
	SigningKey string
	TeamID     string
	KeyID      string
}

func (apple) GetUserByAccessToken(accessToken, sdkType string) (user model.Apple, err error) {
	config := SDKType{
		ClientID:   "",
		SigningKey: "",
		TeamID:     "",
		KeyID:      "",
	}

	if err != nil {
		return
	}
	clientSecret, err := iapple.GenerateClientSecret(config.SigningKey, config.TeamID, config.ClientID, config.KeyID)
	if err != nil {
		return
	}
	client := iapple.New()
	req := iapple.AppValidationTokenRequest{
		ClientID:     config.ClientID,
		ClientSecret: clientSecret,
		Code:         accessToken,
	}
	var resp iapple.ValidationResponse
	err = client.VerifyAppToken(context.Background(), req, &resp)
	if err != nil {
		return
	}

	user, err = parseUserApple(resp)
	return
}

func parseUserApple(resp iapple.ValidationResponse) (user model.Apple, err error) {
	// Get the unique user ID
	unique, err := iapple.GetUniqueID(resp.IDToken)
	if err != nil {
		return
	}

	user.Uid = unique
	user.RefreshToken = resp.RefreshToken
	user.AccessToken = resp.AccessToken
	claims, err := iapple.GetClaims(resp.IDToken)
	if err != nil {
		return
	}
	if claims != nil {
		if emailInterface, ok := (*claims)["email"]; ok {
			if email, ok := emailInterface.(string); ok {
				user.Email = email
			}
		}
		if isPrivateEmailInterface, ok := (*claims)["is_private_email"]; ok {
			if isPrivateEmail, ok := isPrivateEmailInterface.(string); ok {
				if isPrivateEmail == "true" {
					user.IsPrivateEmail = true
				}
			}
		}
		if expiredAtInterface, ok := (*claims)["exp"]; ok {
			if expiredAtUnix, ok := expiredAtInterface.(float64); ok {
				user.ExpiredAt = time.Unix(int64(expiredAtUnix), 0)
			}
		}
	}
	return
}
