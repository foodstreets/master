package model

import "time"

type EateryAddress struct {
	tableName struct{} `sql:"eatery_address,alias:eatery_address" pg:",discard_unknow_columns"`
	Id        int
	EateryId  int
	Lat       float64
	Lng       float64
	CreatedAt time.Time
	UpdatedAt time.Time
	CreatedBy int
	UpdatedBy int
}
