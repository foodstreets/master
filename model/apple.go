package model

import "time"

type Apple struct {
	Uid            string
	LastName       string
	FirstName      string
	Email          string
	Name           string
	RefreshToken   string
	AccessToken    string
	IsPrivateEmail bool
	ExpiredAt      time.Time
}
