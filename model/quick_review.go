package model

import "time"

const (
	ExpiredTimeKeyQuickReview = time.Second * time.Duration(365*24*60*60)
	KeyRedisQuickReview       = "QUICKREVIEW"
)

type QuickReview struct {
	tableName struct{} `sql:"quick_reviews,alias:quick_reviews" pg:",discard_unknow_columns"`
	Id        int
	TypeName  string
	Image     string
	CreatedAt time.Time
	UpdatedAt time.Time
}
