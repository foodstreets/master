package model

import "time"

type UserAccount struct {
	tableName      struct{} `sql:"user_account,alias:user_account" pg:",discard_unknow_columns"`
	Id             int
	Uid            string
	Username       string
	ProfileUrl     string
	AccessToken    string
	RefreshToken   string
	CountryCode    string
	PhoneNumber    string
	CreatedAt      time.Time
	UpdatedAt      time.Time
	LastLoginAt    time.Time
	UserId         int
	Status         int
	Provider       string
	TokenExpiredAt time.Time
}
