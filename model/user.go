package model

import "time"

type User struct {
	tableName           struct{} `sql:"users,alias:users" pg:",discard_unknow_columns"`
	Id                  int
	Username            string
	Avatar              string
	Last                string
	Middle              string
	First               string
	Gender              string
	Email               string
	PhoneNumber         string
	Birthday            string
	Timezone            time.Time
	Street              string
	CreatedAt           time.Time
	UpdatedAt           time.Time
	CreatedBy           int
	UpdatedBy           int
	Status              string
	CityId              int
	Active              bool
	Password            string
	Device              string
	Platform            string
	VerifiedPhoneNumber bool
	TokenHash           string
}
