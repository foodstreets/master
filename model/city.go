package model

type City struct {
	Id   int
	Name string
	Slug string
}
