package model

type Facebook struct {
	Uid        string
	LastName   string
	MiddleName string
	FirstName  string
	Gender     string
	Email      string
	Birthday   string
	Name       string
	NameFormat string
}
