package model

import (
	"time"

	"github.com/lib/pq"
)

type EateryImage struct {
	tableName struct{} `sql:"eatery_images,alias:eatery_images" pg:",discard_unknow_columns"`
	Id        int
	EateryId  int
	Images    pq.StringArray
	CreatedAt time.Time
	UpdatedAt time.Time
	CreatedBy int
	UpdatedBy int
}
