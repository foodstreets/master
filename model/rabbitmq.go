package model

import "time"

const (
	PrefixRabbitmq = "PRERabbitMQ#"

	//Define Action Discovery
	ActionNewDiscovery   = "new_discovery"
	ActionCheckDiscovery = "check_discovery"

	//Define Action Eatery

	//Define Action Dish

)

type RabbitMQ struct {
	Action     string
	Task       string
	Msg        string
	Id         int
	ExecutedAt time.Time
}
