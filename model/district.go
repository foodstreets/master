package model

type District struct {
	Id     int
	CityId int
	Name   string
}
