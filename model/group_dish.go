package model

import (
	"time"
)

type GroupDish struct {
	tableName struct{} `sql:"group_dishes,alias:group_dishes" pg:",discard_unknow_columns"`
	Id        int
	Name      string
	Slug      string
	EateryId  int
	Active    bool
	CreatedAt time.Time
	UpdatedAt time.Time
	CreatedBy int
	UpdatedBy int
}
