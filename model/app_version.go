package model

import "time"

const (
	ExpiredTimeKey     = time.Second * time.Duration(365*24*60*60)
	KeyRedisAppVersion = "APPVERSION"

	AppUserTypeNormal      = "normal"
	AppUserTypeUpdate      = "update"
	AppUserTypeForceUpdate = "force_update"
)

type AppVersion struct {
	tableName   struct{} `sql:"app_versions,alias:app_verions" pg:",discard_unknow_columns"`
	Id          int
	Version     string
	Title       string
	Message     string
	Type        string
	BuildNumber int
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
