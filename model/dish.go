package model

import "time"

type Dish struct {
	tableName   struct{} `sql:"dishes,alias:dishes" pg:",discard_unknow_columns"`
	Id          int
	Name        string
	Slug        string
	Image       string
	GroupDishId int
	Price       float32
	Active      bool
	CreatedAt   time.Time
	UpdatedAt   time.Time
	CreatedBy   int
	UpdatedBy   int
}
