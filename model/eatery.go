package model

import "time"

type Eatery struct {
	tableName  struct{} `sql:"eateries,alias:eateries" pg:",discard_unknow_columns"`
	Id         int
	Name       string
	Slug       string
	Active     bool
	Address    string
	Image      string
	CityId     int
	DistrictId int
	OpenTime   time.Time
	CloseTime  time.Time
	CreatedAt  time.Time
	UpdatedAt  time.Time
	CreatedBy  int
	UpdatedBy  int
}
